
# Version Management with Git

## Prerequisite

### Git BASH
You need to have git already installed. Open a terminal and type:
```
git
```
On Windows, check if you have Git BASH already installed > https://confluence.devnet.klm.com/x/a6EfBw

### VSCode IDE
You need Visual Studio Code and some plugins:
1. Go to https://code.visualstudio.com/ to download and then install VSCode.
2. Add https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph extension.
3. Use Git BASH in VSCode https://confluence.devnet.klm.com/questions/325282425/how-do-i-use-git-bash-on-windows-from-the-visual-studio-code-integrated-terminal

> By default VSCode has an embedded Git Client and a lot of extensions for Git exist.
> More about Git in VSCode at https://code.visualstudio.com/docs/editor/versioncontrol

### Markdown
[Markdown](https://fr.wikipedia.org/wiki/Markdown) files will be used for the exercises ([Play with markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)).

---

## Initialization

### Configure Git on your workstation. 

#### 1. Git help

```bash
# git help
git --help
# git help for a command
git <command> --help
git config --help
```

#### 2. Set who am i

```bash
git config --global user.name "Doe, John (DI NB IC) - AF"
git config --global user.email "jodoe@airfrance.fr"
```

#### 3. Change your default editor

In case of you don't like vim:

```bash
# Set nano as default editor
git config --global core.editor "nano"
```
or
```bash
# Set vs code as default editor
git config --global core.editor "code --wait"
```
More details if you want to set [VS Code as Git editor and as Git diff tool](https://code.visualstudio.com/docs/editor/versioncontrol#_vs-code-as-git-editor)

#### 4. STS / Eclipse

In case you are using EGit client in STS / Eclipse, check:
- installation: https://confluence.devnet.klm.com/x/AzdkBw
- configuration: https://confluence.devnet.klm.com/x/lKaGBg

### Understand Git config
Git config allows to configure git with your settings. There are 3 levels of configuration: local, global and system. The local config overwrites the global config which overwrites the system config.

> To modify your configuration you can use the git config command [git config](https://git-scm.com/docs/git-config) or [edit the configuration file for each level](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup).

#### List all variables

Your configuration:
```
git config -l
```

Same for each levels:
```
git config --local -l
git config --global -l
git config --system -l
```

### Edit the config files

Edit the config file and locate them on your workstation
```
git config --local -e
git config --global -e
git config --system -e
```

#### Overwrite a system variable

```bash
# visualize the variable
git config --get-all core.autocrlf
git config --get core.autocrlf

# set the variable
git config --global core.autocrlf true

# visualize the variable again
git config --get-all core.autocrlf
git config --get core.autocrlf

# unset the variable
git config --global --unset core.autocrlf
```

> Finally check that the variable is set to false otherwise force it
> ```bash
> git config --global core.autocrlf false
> ```

#### Add alias to config

A Git alias is a shortcut to a command.

Examples of useful aliases:

```bash
git config --global alias.l "log --graph --all --remotes --branches --pretty=format:'%C(yellow)%h%C(cyan)%d%Creset %s %C(white)- %an, %ar%Creset'"
# Usage
git l

git config --global alias.lg "log --color --graph --pretty=format:'%C(bold white)%h%Creset -%C(bold green)%d%Creset %s %C(bold green)(%cr)%Creset %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
# Usage
git lg

git config --global alias.ll "log --stat --abbrev-commit"
# Usage
git ll

git config --global alias.llg "log --color --graph --pretty=format:'%C(bold white)%H %d%Creset%n%s%n%+b%C(bold blue)%an <%ae>%Creset %C(bold green)%cr (%ci)' --abbrev-commit"
# Usage
git llg

```

---

## Git basics

### Clone the repository
Use [git clone](https://git-scm.com/docs/git-clone) command by default a directory named as the repository name is created and contained the local repository:
```bash
# create an empty directory (named for example git) on your workstation
mkdir git
cd git/
# clone the repository
git clone https://bitbucket.devnet.klm.com/scm/gitraining/git-training.git
# navigate in the local git repository
cd git-training/
```

> ***TIP***: Specify the destination directory with git clone
> ```bash
> # The destination directory is toto
> git clone https://bitbucket.devnet.klm.com/scm/gitraining/git-training.git toto
> ```
> ---

### Check your status
Use [git status](https://git-scm.com/docs/git-status) command:
```bash
git status
```

### Manipulate a file locally

The schema below illustrates the manipulation that will be done ([more details](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)):

![logo](https://git-scm.com/book/en/v2/images/lifecycle.png)

#### 1. Ignore file and clean untracked file

> Commands needed:
> - [git status](https://git-scm.com/docs/git-status)
> - [git clean](https://git-scm.com/docs/git-clean)

```bash
# check the status
git status
# create a markdown file (replace below jodoe by your email prefix before the @)
echo 'empty' > ignore_me.txt
# check the status, nothing change (file is ignore)
git status
# see the gitignore file
cat .gitignore
# rename the file
echo 'empty' > not_ignore_me.txt
# check the status, file is not ignored anymore
git status
# clean untracked file / remove the file and directories
git clean -fd 
ls -al
```

> ***MORE***: You can edit and remove the ignore_me.txt file

#### 2. Create and add a file locally

> Commands needed:
> - [git add](https://git-scm.com/docs/git-add)
> - [git commit](https://git-scm.com/docs/git-commit)



> ---
> **NOTE**:
> 
> In all the exercices when there are a 'jodoe' reference, replace by your > email prefix. Ex.: Bruce Wayne will replace jodoe.md by brwayne.md
> 
> ---


```bash
# create a markdown file (replace below jodoe by your email prefix before the @)
echo '### John Doe' > jodoe.md
# check the status
git status

# add the file to staged
git add jodoe.md
# check the status
git status

# commit with comment
git commit -m "create new jodoe.md file"
# check the status
git status
```

> ***TIP***: Add all untracked files
> ```bash
> git add --all
> # or
> git add .
> ```
> ---

#### 3. Modify the file a file locally

```bash
# modify/edit the file
echo 'is an unknown man' >> jodoe.md
# check the status
git status
# add the modification of the file to staged
git add jodoe.md

# commit with comment
git commit -m "modify jodoe.md file"
# check the status
git status
```

#### 4. Consult the history 

> Commands needed:
> - [git log](https://git-scm.com/docs/git-log)
> - [git show](https://git-scm.com/docs/git-show)
> - [git diff](https://git-scm.com/docs/git-diff)

```bash

# consult history
git log
git show

# comparison between local (staged and workspace) and HEAD 
# !!! TODO > git diff mixed do not exist use so git status -vv !!!
git status -vv
echo 'more info about jodoe' >> jodoe.md 
git status -vv
git add jodoe.md
git status -vv

# remove a file from index 
git reset jodoe.md
git status -vv
# add it again
git add jodoe.md

# show commits 
git log --oneline

# comparison between workspace and a commit (get with the command above)
git diff <commit1>
git diff <commit2>

# comparison between two commits (use commit above)
git diff <commit1> <commit2>
```

#### 5. Reset modification of a file

> Command needed:
> - [git checkout](https://git-scm.com/docs/git-checkout)

```bash
git status
git checkout master jodoe.md
git status
```

#### 6. Untrack the file

> Command needed:
> - [git rm](https://git-scm.com/docs/git-rm)

```bash
# untrack the file
git rm --cached jodoe.md
# check the status
git status

# commit with comment
git commit -m "untrack jodoe.md file"
# check the status
git status

# track again the file
git add jodoe.md
git commit -m "jodoe.md file is back"
```

#### 7. Remove the file

```bash
# untrack the file
git rm jodoe.md
# check the status
git status

# commit with comment
git commit -m "delete jodoe.md file"
# check the status
git status
```

> ***TIP***: Delete the file is the same as the ***git rm*** command
> ```bash
> rm jodoe.md
> ```
> ---


#### 8. Stash and amend commit

1. Save the file (stash)

```bash
# create a new file
echo one > jodoe_one.txt
# do not commit, save for later (-u for include untracked files)
git stash -u
# the file is not present anymore
ls -al
# list stash
git stash list
```

> ***TIP***: Name the stash has 'toto'
> ```bash
> git stash save -u toto
> ```
> ---


2. Amend commit (stash)

```bash
# watch the current commits
git log --oneline
git show
# create a new file
echo 'amend file' > jodoe_amend.txt
git add jodoe_amend.txt
# add the file to the command without modifying the commit message (--no-edit)
git commit --amend --no-edit
# watch the current commits
git log --oneline
git show
```

> When you amend a commit the id of the commit change.

3. Get what you saved

```bash
git stash pop
ls -al
```

#### 9. Reset and clean

1. Reset all your commits

Reset commits and go back to the reference origin/master
```bash
git log --oneline

# Reset all
git reset --hard origin/master
# Finish the cleaning
git clean -fd

ls -al

git log --oneline
```

2. Restore the commits

You've made a mistake, you want to restore the commits. For that you have reset by using the reference ORIG_HEAD.

```bash
git reset --hard ORIG_HEAD
git log --oneline
```

3. Remove last commit

```bash
# reset last commit
git reset --soft head^
# check the files
git status
git reset --hard ORIG_HEAD
```

4. Squash all in one commit

```bash
git log --oneline

# reset all commits from reference origin/master
git reset --soft origin/master
# check the files
git status

git add --all
git commit -m "result of squashed commits"

git log --oneline
```

> ***TIP***: You can try with git reset --mixed, the different with --soft is that
> the index is reset.
> ```bash
> git reset = git reset --mixed
> ```

5. Add a tag

> Command needed:
> - [git tag](https://git-scm.com/docs/git-tag)

```bash
git log --oneline
git tag v1.0 master
# check that the tag was added
git log --oneline
```

6. Clean all before working in team
```bash
git log --oneline

git reset --hard origin/master

git log --oneline
# in addition you can check the diff between master and v1.0
git diff master..v1.0
```

---

## Work in team

<a name="update-your-local-repository-with-remote-repository-data"></a>

### 1. Update your local repository with remote repository data

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> Commit a file on the main branch, then ask trainees to fetch
>
> ```bash
> echo 'from the trainer' > trainer.txt
> git add trainer.txt
> git commit -m 'add trainer.txt'
> git push origin master
> ```
>
> </details>

> Commands needed:
> - [git fetch](https://git-scm.com/docs/git-fetch)
> - [git merge](https://git-scm.com/docs/git-fetch)

```bash
# check the difference between your master branch and the origin/master
git diff origin/master
git log --oneline

# fetch the modification from the remote repository and update your local repository
git fetch

# check again to see the modifications
git diff origin/master
git log --oneline

# merge the modifications
git merge origin/master
git log --oneline
```

### 2. Update your current branch with a commit from the tag v1.0

> Command needed:
> - [git cherry-pick](https://git-scm.com/docs/git-cherry-pick)

```bash
# check the difference
git diff v1.0
git log --oneline

# pick this commit to update master
git cherry-pick v1.0

# check the difference
git diff v1.0
git log --oneline
```

### 3. Synchronize your work with others trainees

> Command needed:
> - [git pull](https://git-scm.com/docs/git-pull)

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> Commit a file on the main branch, then ask trainees to fetch
>
> ```bash
> echo 'another line!!!' >> trainer.txt
> git add trainer.txt
> git commit -m 'add another line in trainer.txt'
> git push origin master
> ```
>
> </details>

```bash
# try to push your work on the remote repository
git push origin master
```

> ***TIP***:
> ```bash
> git push origin master
> # or simply if you already have done it.
> git push
> ```
> ---

To solve a rejected push, you have to resynchronized your local repository with the remote like [Update your local repository with remote repository data](#update-your-local-repository-with-remote-repository-data).

```bash
# synchronize
git pull origin master
# and push again
git push origin master
```

> ***TIP***:
> ```bash
> git pull = git fetch + git merge
> ```
> ---

***RESULT***:
The result of multiple synchronizations should be a graph with some merge commits:

![synchro](resources/git-merge.png)

### 4. Synchronize your work with others trainees with conflict

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> Add in trainer.txt file on each line the name of trainees
>
> ```bash
> echo 'trainee1' >> trainer.txt
> echo 'trainee2' >> trainer.txt
> echo 'trainee3' >> trainer.txt
> echo 'trainee4' >> trainer.txt
> git add trainer.txt
> git commit -m 'add names in trainer.txt'
> git push origin master
> ```
>
> </details>

```bash
# Retrieve the trainer.txt file from the remote
git pull origin master

# Modify the line with your name by adding something at the end
vim trainer.txt
# Add something

git add trainer.txt
git commit -m 'trainee x add something'
```

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> Modify each line of trainer.txt to prefix with aaa each trainees' name
>
> ```bash
> # Add aaa to each name in trainer.txt
> # ex.: aaa trainee1
> vim trainer.txt
> git add trainer.txt
> git commit -m 'modify trainer.txt'
> git push origin master
> ```
>
> </details>

```bash
# Retrieve the trainer.txt file from the remote
git pull origin master
# Check the file in conflict
git status
git diff
```

> You have a **CONFLICT**

> ***TIP***: In a file with conflicts, there's some information added by git to show you a conflict like in the example below:
> ```bash
> ++<<<<<<< HEAD
> +man1!
> ++=======
> + yoman1
> ++>>>>>>> e3def26848fb883b4f775529ca83e1a7c1fa8444
> ```
> ---

### 5. Resolve a conflict

```bash
# Edit the file in conflict
vim trainer.txt
# Suppress what you don't need and keep what you need.

# Mark the conflict resolved
git add trainer.txt

git commit -m 'conflict resolved'

# Do several time the actions below to get the modification of other trainees.
git push origin master
git pull origin master
```

> [Tip for easy merge :)](https://geekandpoke.typepad.com/.a/6a00d8341d3df553ef0134885909e0970c-pi)

### 6. Work on a branch

> **Be sure to be synchronized with the latest modification from the remote (git pull)**


> Commands needed:
> - [git branch](https://git-scm.com/docs/git-branch)
> - [git switch](https://git-scm.com/docs/git-switch)

```bash
# List the branches
git branch

# Create and go to the new branch (name it as your email prefix)
# 'git switch -c' is the same as 'git checkout -b jodoe'
git switch -c joedoe

# Visualize the new branch
git branch

# Create a file different from other trainee with your matricule
touch mxxx.txt
git add mxxx.txt
git commit -m "add mxxx.txt"


# Modify the trainer.txt file at the line with your name
vim trainer.txt
git add trainer.txt
git commit -m "add line in trainer.txt"

# Watch the graph, there is 2 commits on the branch

```

### 7. Rebase your branch onto master

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> On master branch, modify each line of trainer.txt to prefix with bbb each trainees' name
>
> ```bash
> # Add bbb to each name in trainer.txt
> # ex.: bbb trainee1
> vim trainer.txt
> git add trainer.txt
> git commit -m 'modify trainer.txt'
> git push origin master
> ```
>
> </details>

> Command needed:
> - [git rebase](https://git-scm.com/docs/git-pull)

```bash
# Switch to master
git switch master

# Synchronize
git pull origin master

# Check that you are in the branch you want to rebase
git branch

# If not switch/checkout in it
git switch <branch_name>

# Start the rebase
git rebase master

# To resolve a conflict
# Edit the file and choose what to keep
vim trainer.txt
# Solve the conflict
git add trainer.txt
# Continue the rebase.
# In a rebase DO NOT COMMIT for resolving the conflict.
git rebase --continue

# Check the graph you should see the commits from the branch after the trainer commit.

# To integrate your branch in master
git switch master
# The merge is fast forward after the rebase
git merge <your_branch>

# Each trainee can push and pull
git push
git pull

# Check the graph... It's linear
```

---

## Git with Jira and Bitbucket

> **Wait for trainer instructions before continuing the exercises.**
>
> To do by trainer only:
> <details>
>
> Remove all issue of the Jira project:
> - Go to [Jira Project GITRAINING](https://jira.devnet.klm.com/browse/GITRAINING-228?jql=project%20%3D%20GITRAINING%20)
> - Click the Tools (Gear) button at the upper right corner.
> - Select all issue(s) of the 'Bulk Change'
> - Do the 'Bulk Operation' Check all issue(s) with 'Delete Issues' operation
>
> </details>

1. Go to [Jira Project GITRAINING](https://jira.devnet.klm.com/projects/GITRAINING/issues/?filter=allopenissues) .
2. Create an issue to this project with your name in the summary.
3. Assign to yourself the issue.
4. Open the issue and create a feature branch using the 'Create branch' button at the bottom right in 'Development'.
5. You are redirected in Bitbucket in the menu 'Create branch... '.
6. In the menu:
    - select the **'Repository'** in the [GITRAINING](https://bitbucket.devnet.klm.com/projects/GITRAINING/repos/git-training/browse) project.
    - select the **'Branch type'** corresponding to your 'Jira issue type'.
    - select in **'Branch from'** the 'master' branch.
    - Let the default **'Branch name'** build by Bitbucket.
7. Work in this branch:
    ```bash
    # Fetch the branch to see it in your local repository.
    git fetch
    # Create the local branch to work in
    git checkout <your_branch_name>
    # Ex.: git checkout bugfix/GITRAINING-229-my-branch
    ```
8. Create some commits in your branch and **push them**.
9. In the project in Bitbucket, create a 'pull request' to be reviewed by all the trainees.
10. All trainees review the pull request.
11. When the request is accepted, merge it in the master branch using the merge button in the pull request interface of Bitbucket.

> See with the trainer to create a conflict and solve it in the pull request.
